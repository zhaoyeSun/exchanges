package com.example.demo.mapper;

import com.example.demo.base.ActivityInfoCodeBean;
import com.example.demo.base.ExpressInfoBean;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by cityre on 2017/11/9.
 */
public interface ActivityInfoMapper {

    String queryData(@Param("code")String code);

    ExpressInfoBean queryExpress(@Param("mobile")String mobile);

    void updateBatch(List list);

    void deleCode(@Param("code")String code);


    List<ExpressInfoBean> exportExcel(@Param("staTime")String staTime,@Param("endTime")String endTime);

    int insertResult(ExpressInfoBean expressInfoBean);


    int insertCode(List<ActivityInfoCodeBean> list);

    ExpressInfoBean selectInfo(@Param("id") int id);

    /**
     * 清空所有记录，并重新定义index
     */
    @Select("DELETE FROM ha_market_error_info where datatype = 2 AND sale_or_rent = 2")
    void deleteAll();
}

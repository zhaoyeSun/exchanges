package com.example.demo.works;


import com.example.demo.base.ActivityInfoCodeBean;
import com.example.demo.base.ExpressInfoBean;
import com.example.demo.mapper.ActivityInfoMapper;
import com.example.demo.tools.ExcelTools;
import com.google.common.base.Strings;
import net.minidev.json.JSONObject;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.example.demo.tools.ExcelTools.exportExcelFile;

/**
 * Created by cityre on 2017/11/16.
 */

@RestController
@RequestMapping("/search/data/")
public class ActivityController {

    final static private Logger log = LoggerFactory.getLogger(ActivityController.class);

    @Autowired
    private ActivityInfoMapper activityInfoMapper;






    /**
     * 查询用户快递信息根据手机号
     * @param mobile 手机号
     * @return String
     */
    @RequestMapping(value = "queryDelivery/{mobile}",method = RequestMethod.GET)
    public String queryDelivery(@PathVariable("mobile")String mobile){
        JSONObject json = new JSONObject();
        ExpressInfoBean e = activityInfoMapper.queryExpress(mobile);
        if (null == e) {
            json.put("status", 500);
            json.put("resutl", "没有查询到该用户，请核实手机号.");
            return json.toJSONString();
        }
        if (Strings.isNullOrEmpty(e.getExpress_order())) {
            json.put("status", 500);
            json.put("resutl", "该用户还未录入物流信息.");
            return json.toString();
        }
        json.put("status", 200);

        json.put("resutl", e);
        return json.toString();
    }

    /**
     * 查询兑换码  在那个区间，返回区间标签
     * @param activity_code
     * @return
     */
    @RequestMapping(value = "query/{activity_code}",method = RequestMethod.GET)
    public String query(@PathVariable("activity_code")String activity_code){
        JSONObject json = new JSONObject();
        String result = activityInfoMapper.queryData(activity_code);
        if (Strings.isNullOrEmpty(result)) {
            json.put("status", 500);
            json.put("resutl", "该兑换码无效，请重新输入.");
            return json.toString();
        }
        json.put("status", 200);
        json.put("resutl", result);
        return json.toString();
    }

    /**
     * 插入用户数据
     * @param expressInfoBean
     * @return
     */
    @Transactional(value = "supportTransaction")
    @RequestMapping(value = "insert",method = RequestMethod.POST)
    public String insert(@RequestBody ExpressInfoBean expressInfoBean){
        JSONObject json = new JSONObject();
        /*if (!checkChinaPhone(expressInfoBean.getMobile())){
            json.put("status", 500);
            json.put("result", "手机号错误请重新输入");
            return json.toString();
        }*/
        expressInfoBean.setUpdate_time(new Date());
        activityInfoMapper.insertResult(expressInfoBean);
        activityInfoMapper.deleCode(expressInfoBean.getActivity_code());
        ExpressInfoBean e = activityInfoMapper.selectInfo(expressInfoBean.getId());
        json.put("status", 200);
        json.put("result", e);
        return json.toString();
    }

    /**
     * 批量导入兑换码 通过EXCEL
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "excelImportcode",method = RequestMethod.POST,produces="application/json;charset=UTF-8",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String importExelCode (
            HttpServletRequest request,
            HttpServletResponse response
    )throws IOException {
        JSONObject json = new JSONObject();
        //将当前上下文初始化给  CommonsMutipartResolver （多部分解析器）
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());

        //检查form中是否有enctype="multipart/form-data"
        if (multipartResolver.isMultipart(request)) {
            //将request变成多部分request
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            //获取multiRequest 中所有的文件名
            Iterator<String> iter = multiRequest.getFileNames();
            List<ActivityInfoCodeBean> list = null;
            while (iter.hasNext()) {
                //一次遍历所有文件
                MultipartFile file = multiRequest.getFile(iter.next().toString());
                InputStream is = file.getInputStream();
                list = ExcelTools.getAllByExcel(is);
            }
            if (list != null && list.size()>0) {
                int rows = activityInfoMapper.insertCode(list);
                if (rows > 0) {
                    json.put("status", 200);
                    json.put("result", "导入成功，共导入"+rows+"条");
                    return json.toString();
                }
            }

        }
        json.put("status", 500);
        json.put("result", "导入失败，请检查数据格式或者是否兑换码重复");
        return json.toString();
    }

    /**
     * 导出数据到Excel
     * @param staTime
     * @param endTime
     * @param rep
     * @param req
     */
    @RequestMapping(value = "exportExcel/{staTime}/{endTime}",method = RequestMethod.GET)
    public void exportExcel( @PathVariable("staTime")String staTime,@PathVariable("endTime")String endTime,
            HttpServletResponse rep, HttpServletRequest req
    ){
        List<ExpressInfoBean> eib = activityInfoMapper.exportExcel(staTime, endTime);
        if (eib != null) {
            Map<String,Object> map = new HashMap();
            map.put("vms", eib);
            InputStream is = null;
            String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println(req.getSession().getServletContext().getRealPath("/"));

            try {
                is = new FileInputStream("/root/home/server/t.xls");
                XLSTransformer transformer = new XLSTransformer();
                Workbook workbook = transformer.transformXLS(is, map);
                String xlsName = String.format("用户详情-%s.xls", sf.format(new Date()));
                exportExcelFile(xlsName, rep, workbook);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }catch (InvalidFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 批量更新 用户  快递公司 和 快递单号
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @Transactional(value = "supportTransaction")
    @RequestMapping(value = "excelUpdateData",method = RequestMethod.POST,produces="application/json;charset=UTF-8",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String excelUpdateData(HttpServletRequest request, HttpServletResponse response)throws IOException{
        JSONObject json = new JSONObject();
        //将当前上下文初始化给  CommonsMutipartResolver （多部分解析器）
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());

        //检查form中是否有enctype="multipart/form-data"
        if (multipartResolver.isMultipart(request)) {
            //将request变成多部分request
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            //获取multiRequest 中所有的文件名
            Iterator<String> iter = multiRequest.getFileNames();
            List<ExpressInfoBean> list = null;
            while (iter.hasNext()) {
                //一次遍历所有文件
                MultipartFile file = multiRequest.getFile(iter.next().toString());
                InputStream is = file.getInputStream();
                list = ExcelTools.getUserByExcel(is);
            }
            if (list != null) {
/*                for (ExpressInfoBean aic : list) {
                    System.out.println("id:"+aic.getId() + "     order:"+aic.getExpress_order() +"  company:"+aic.getExpress_company());
                }*/
                activityInfoMapper.updateBatch(list);
                json.put("status", 200);
                json.put("result", "更新成功，共更新"+list.size()+"条.");
                return json.toString();
            }
        }
        json.put("status", 500);
        json.put("result", "更新失败，请检查数据格式");
        return json.toString();

    }
}

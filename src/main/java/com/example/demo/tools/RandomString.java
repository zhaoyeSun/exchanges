package com.example.demo.tools;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Created by cityre on 2017/11/17.
 */
public class RandomString {

    /***
     * 产生随机数的方法
     *
     * @param length
     * @return
     */
    public static String getCharAndNumr(int length) {
        if (length >= 3) {
            String val = "";
            Random random = new Random();
            // t0、t1、t2用来标识大小写和数字是否在产生的随机数中出现
            int t0 = 0;
            int t1 = 0;
            int t2 = 0;
            for (int i = 0; i < length; i++) {
                String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num"; // 输出字母还是数字
                // 产生的是字母
                if ("char".equalsIgnoreCase(charOrNum)) {
                    //取得大写字母还是小写字母
                    int choice = 0;
                    if (random.nextInt(2) % 2 == 0) {
                        choice = 65;
                        t0 = 1;
                    } else {
                        choice = 97;
                        t1 = 1;
                    }
                    val += (char) (choice + random.nextInt(26));
                } else if ("num".equalsIgnoreCase(charOrNum)) {
                    val += String.valueOf(random.nextInt(10));
                    t2 = 1;
                }
            }
            // 用于判断是是否包括大写字母、小写字母、数字
            if (t0 == 0 || t1 == 0 || t2 == 0) {
                val = getCharAndNumr(length); // 不满足则递归调用该方法
                return val;
            } else
                return val;

        } else {

            return null;
        }
    }


    /****
     * 主函数方法
     *
     * @param args
     */
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            String rcs = getCharAndNumr(12); // 调用随机数的方法
            list.add(rcs);
        }
        HashSet h = new HashSet(list);
        list.clear();
        list.addAll(h);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}

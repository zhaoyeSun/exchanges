package com.example.demo.tools;

import com.example.demo.base.ActivityInfoCodeBean;
import com.example.demo.base.ExpressInfoBean;
import com.google.common.base.Strings;
import jxl.Sheet;
import jxl.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cityre on 2017/11/17.
 */
public class ExcelTools {

    private static final String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel;charset=utf-8";

    /**
     * 查询指定目录中电子表格中所有的数据
     * @return
     */
    public static List getAllByExcel(InputStream is){
        List list = new ArrayList();
        try {
            Workbook rwb= Workbook.getWorkbook(is);
            Sheet rs=rwb.getSheet(0);
            int rows=rs.getRows();//得到所有的行
            for (int i = 1; i < rows; i++) {
                for (int j = 0; j < 1; j++) {
                    //第一个是列数，第二个是行数
                    String code=rs.getCell(j++, i).getContents();//默认最左边编号也算一列 所以这里得j++
                    String section=rs.getCell(j++, i).getContents();
                    if (code.length() >= 6 && code.length() <= 12) {
                        list.add(new ActivityInfoCodeBean(code,section));
                    }
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    public static List getUserByExcel(InputStream is){
        List list = new ArrayList();
        try {
            Workbook rwb= Workbook.getWorkbook(is);
            Sheet rs=rwb.getSheet(0);
            int clos=rs.getColumns();//得到所有的列
            int rows=rs.getRows();//得到所有的行
            for (int i = 1; i < rows; i++) {
                for (int j = 0; j < 1; j++) {
                    //第一个是列数，第二个是行数
                    String id = rs.getCell(j++, i).getContents();//默认最左边编号也算一列 所以这里得j++
                    String code = rs.getCell(clos - 1, i).getContents();
                    String company = rs.getCell(clos - 2, i).getContents();
                    if (!Strings.isNullOrEmpty(code) && !Strings.isNullOrEmpty(company)) {
                        /*ExpressInfoBean e = new ExpressInfoBean();
                        e.setId(Integer.parseInt(id));
                        e.setExpress_order(code);
                        e.setExpress_company(company);*/
                        list.add(new ExpressInfoBean(Integer.parseInt(id), code, company));
                    }
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }






    public static void exportExcelFile(String fileName, HttpServletResponse response,
                                       org.apache.poi.ss.usermodel.Workbook workbook) throws IOException{
        setResponseHeader(fileName, CONTENT_TYPE_EXCEL, response);
        workbook.write(response.getOutputStream());
    }

    public static void setResponseHeader(
            String targetFileName, String contentType,
            HttpServletResponse response) throws UnsupportedEncodingException{
        response.setContentType(contentType);
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition","attachment;filename="+new String(targetFileName.getBytes("utf-8"),"ISO8859-1")+"");
    }

    /**
     * 国内手机号码11位数，匹配格式：前三位固定格式+后8位任意数 此方法中前三位格式有： 13+任意数 15+除4的任意数 18+除1和4的任意数
     * 17+除9的任意数 147
     */
    public static boolean checkChinaPhone(String str) {
        String regExp = "^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }
}

package com.example.demo.tools;


import com.jolbox.bonecp.BoneCPDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;


@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = { "com.example.demo.mapper"}, sqlSessionFactoryRef = "sqlSessionFactoryMap")
public class DataSourceConfig {

	@Autowired
	Environment env;

	@Bean
	public PlatformTransactionManager supportTransaction(@Qualifier("supportDataSource") DataSource supportDataSource) {
		return new DataSourceTransactionManager(supportDataSource);
	}

	@Bean
	public SqlSessionFactory sqlSessionFactoryMap(@Qualifier("supportDataSource") DataSource supportDataSource)
			throws Exception {
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(supportDataSource);
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sqlSessionFactory.setMapperLocations(resolver.getResources("classpath:mybatis/citydb/*.xml"));
		sqlSessionFactory.setConfigLocation(resolver.getResource("classpath:mybatis/config/MainConfig.xml"));
		return sqlSessionFactory.getObject();
	}

	@Bean(name = { "supportDataSource" }, destroyMethod = "close")
	@ConditionalOnMissingBean(value = DataSource.class, name = "supportDataSource")
	public DataSource supportDataSource() {
		BoneCPDataSource dataSource = new BoneCPDataSource();
		dataSource.setDriverClass(env.getProperty("supporting.db.driver-class-name"));
		dataSource.setJdbcUrl(env.getProperty("supporting.db.url"));
		dataSource.setUsername(env.getProperty("supporting.db.username"));
		dataSource.setPassword(env.getProperty("supporting.db.password"));
		dataSource.setIdleConnectionTestPeriodInMinutes(60);
		dataSource.setIdleMaxAgeInMinutes(240);
		dataSource.setMaxConnectionsPerPartition(10);
		dataSource.setMinConnectionsPerPartition(1);
		dataSource.setPartitionCount(2);
		dataSource.setAcquireIncrement(5);
		dataSource.setStatementsCacheSize(100);
		dataSource.setInitSQL("SET NAMES 'utf8mb4'");
		return dataSource;
	}

}

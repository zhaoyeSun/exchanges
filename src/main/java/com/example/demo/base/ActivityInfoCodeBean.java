package com.example.demo.base;

/**
 * Created by cityre on 2017/11/16.
 */
public class ActivityInfoCodeBean {

    public ActivityInfoCodeBean(String activity_code, String section) {
        this.activity_code = activity_code;
        this.section = section;
    }
    public ActivityInfoCodeBean(){
        this.id = id;
        this.activity_code = activity_code;
        this.section = section;
    }

    private int id;

    private String activity_code;

    private String section;

    private int use;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActivity_code() {
        return activity_code;
    }

    public void setActivity_code(String activity_code) {
        this.activity_code = activity_code;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public int getUse() {
        return use;
    }

    public void setUse(int use) {
        this.use = use;
    }
}

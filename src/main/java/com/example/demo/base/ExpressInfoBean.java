package com.example.demo.base;

import java.util.Date;

/**
 * Created by cityre on 2017/11/16.
 */
public class ExpressInfoBean {

    public ExpressInfoBean(int id,String express_order,String express_company){
        this.id = id;
        this.express_order = express_order;
        this.express_company = express_company;
    }


    public ExpressInfoBean() {
        this.id = id;
        this.activity_code = activity_code;
        this.nick_name = nick_name;
        this.mobile = mobile;
        this.address = address;
        this.order_id = order_id;
        this.item_name = item_name;
        this.express_order = express_order;
        this.express_company = express_company;
        this.update_time = update_time;
    }

    private int id;

    private String nick_name;

    private String item_name;

    private String mobile;

    private String order_id;

    private String express_order;

    private String express_company;

    private String address;

    private String activity_code;

    private Date update_time;

    public int getId() {
        return id;
    }



    public void setId(int id) {
        this.id = id;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getExpress_order() {
        return express_order;
    }

    public void setExpress_order(String express_order) {
        this.express_order = express_order;
    }

    public String getExpress_company() {
        return express_company;
    }

    public void setExpress_company(String express_company) {
        this.express_company = express_company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getActivity_code() {
        return activity_code;
    }

    public void setActivity_code(String activity_code) {
        this.activity_code = activity_code;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }
}
